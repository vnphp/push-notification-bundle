# Vnphp Push Notification bundle


[![build status](https://gitlab.com/vnphp/push-notification-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/push-notification-bundle/commits/master)


## Installation 

```
composer require vnphp/push-notification-bundle
```

## Usage

```php
<?php

use Vnphp\PushNotificationBundle\Model\Message;

$message = new Message();
$message->setTitle('title')
    ->setBody('body')
    ->setIcon('icon')
    ->setClickAction('click action');

$this->get('vnphp_push_notification.notifier')->notifyUsers($message, 'token');
```
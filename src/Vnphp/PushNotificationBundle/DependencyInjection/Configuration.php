<?php

namespace Vnphp\PushNotificationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('vnphp_push_notification');


        $rootNode->children()
                ->scalarNode('api_key')
                    ->isRequired()
                ->end()
            ->end();
        return $treeBuilder;
    }
}

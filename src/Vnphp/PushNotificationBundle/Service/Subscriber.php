<?php


namespace Vnphp\PushNotificationBundle\Service;

use Buzz\Browser;

class Subscriber
{
    /**
     * @var Browser
     */
    protected $browser;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * Subscriber constructor.
     * @param Browser $browser
     * @param string $apiKey
     */
    public function __construct(Browser $browser, $apiKey)
    {
        $this->browser = $browser;
        $this->apiKey = $apiKey;
    }

    public function subscribeToTopic($token, $topic)
    {
        $url = "https://iid.googleapis.com/iid/v1/$token/rel/topics/$topic";
        $response = $this->browser->post($url, [
            'Content-Type'  => 'application/json',
            'Authorization' => "key={$this->apiKey}",
        ]);
        if (!$response->isSuccessful()) {
            throw new \RuntimeException("{$response->getReasonPhrase()}: {$response->getContent()}");
        }
    }
}

<?php


namespace Vnphp\PushNotificationBundle\Service;

use Buzz\Browser;
use Vnphp\PushNotificationBundle\Model\Message;

class Notifier
{
    /**
     * @var Browser
     */
    protected $browser;

    /**
     * @var string
     */
    protected $apiKey;

    protected $dryRun = false;

    /**
     * Notifier constructor.
     * @param Browser $browser
     * @param string $apiKey
     */
    public function __construct(Browser $browser, $apiKey)
    {
        $this->browser = $browser;
        $this->apiKey = $apiKey;
    }

    /**
     * @param bool $dryRun
     */
    public function setDryRun($dryRun)
    {
        $this->dryRun = $dryRun;
    }

    public function notifyTopic(Message $message, $topic, $ttl = null)
    {
        $content = [
            'notification' => $this->serializeMessage($message),
            'dry_run'      => $this->dryRun,
            'to'           => "/topics/{$topic}",
        ];

        if ($ttl) {
            $content['time_to_live'] = $ttl;
        }

        $this->send($content);
    }

    public function notifyUsers(Message $message, $tokenIds, $ttl = null)
    {
        $tokenIds = (array)$tokenIds;

        $content = [
            'notification'     => $this->serializeMessage($message),
            'dry_run'          => $this->dryRun,
            'registration_ids' => $tokenIds,
        ];
        if ($ttl) {
            $content['time_to_live'] = $ttl;
        }

        $this->send($content);
    }

    /**
     * @param Message $message
     * @return array
     */
    protected function serializeMessage(Message $message)
    {
        return [
            'title'        => $message->getTitle(),
            'body'         => $message->getBody(),
            'icon'         => $message->getIcon(),
            'click_action' => $message->getClickAction(),
        ];
    }

    protected function send(array $content)
    {
        $headers = [
            'Content-Type'  => 'application/json',
            'Authorization' => "key={$this->apiKey}",
        ];

        $response = $this->browser->post('https://fcm.googleapis.com/fcm/send', $headers, json_encode($content));
        if (!$response->isSuccessful()) {
            throw new \RuntimeException("{$response->getReasonPhrase()}: {$response->getContent()}");
        }

        $data = json_decode($response->getContent(), true);
        if (isset($data['failure']) && $data['failure']) {
            $fails = array_filter($data['results'], function (array $result) {
                return array_key_exists('error', $result);
            });
            throw new \RuntimeException(
                "{$data['failure']} notification(s) were not sent:\n" .
                implode("\n", array_map('json_encode', $fails))
            );
        }
    }
}

<?php


namespace Vnphp\PushNotificationBundle\Tests\Service;

use Buzz\Browser;
use Buzz\Message\Response;
use Vnphp\PushNotificationBundle\Model\Message;
use Vnphp\PushNotificationBundle\Service\Subscriber;

class SubscriberTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Browser|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $browser;

    /**
     * @var Subscriber
     */
    protected $subscriber;

    public function testSubscribeToTopic()
    {
        $response = $this->getMockBuilder(Response::class)
            ->getMock();

        $response->method('isSuccessful')
            ->will(static::returnValue(true));

        $this->browser->expects(static::once())
            ->method('post')
            ->will(static::returnValue($response));

        $message = new Message();
        $message->setTitle('title')
            ->setBody('body')
            ->setIcon('icon')
            ->setClickAction('click action');

        $this->subscriber->subscribeToTopic('token', 'test');
    }

    protected function setUp()
    {
        $this->browser = $this->getMockBuilder(Browser::class)
            ->getMock();

        $this->subscriber = new Subscriber($this->browser, '');
    }
}

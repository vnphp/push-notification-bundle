<?php


namespace Vnphp\PushNotificationBundle\Tests\Service;

use Buzz\Browser;
use Buzz\Message\Response;
use Vnphp\PushNotificationBundle\Model\Message;
use Vnphp\PushNotificationBundle\Service\Notifier;

class NotifierTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Browser|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $browser;

    /**
     * @var Notifier
     */
    protected $notifier;

    public function testNotifyUsers()
    {
        $response = $this->getMockBuilder(Response::class)
            ->getMock();

        $response->method('isSuccessful')
            ->will(static::returnValue(true));

        $this->browser->expects(static::once())
            ->method('post')
            ->will(static::returnValue($response));

        $message = new Message();
        $message->setTitle('title')
            ->setBody('body')
            ->setIcon('icon')
            ->setClickAction('click action');

        $this->notifier->notifyUsers($message, 'test');
    }

    public function testNotifyTopic()
    {
        $response = $this->getMockBuilder(Response::class)
            ->getMock();

        $response->method('isSuccessful')
            ->will(static::returnValue(true));

        $this->browser->expects(static::once())
            ->method('post')
            ->will(static::returnValue($response));

        $message = new Message();
        $message->setTitle('title')
            ->setBody('body')
            ->setIcon('icon')
            ->setClickAction('click action');

        $this->notifier->notifyTopic($message, 'test');
    }

    public function testNotifyUsersTtl()
    {
        $response = $this->getMockBuilder(Response::class)
            ->getMock();

        $response->method('isSuccessful')
            ->will(static::returnValue(true));

        $this->browser->expects(static::once())
            ->method('post')
            ->with(
                'https://fcm.googleapis.com/fcm/send',
                static::anything(),
                static::callback(function ($body) {
                    $params = json_decode($body, true);
                    return $params['time_to_live'] === 60;
                })
            )
            ->will(static::returnValue($response));

        $message = new Message();
        $message->setTitle('title')
            ->setBody('body')
            ->setIcon('icon')
            ->setClickAction('click action');

        $this->notifier->notifyUsers($message, 'test', 60);
    }

    public function testNotifyTopicTtl()
    {
        $response = $this->getMockBuilder(Response::class)
            ->getMock();

        $response->method('isSuccessful')
            ->will(static::returnValue(true));

        $this->browser->expects(static::once())
            ->method('post')
            ->with(
                'https://fcm.googleapis.com/fcm/send',
                static::anything(),
                static::callback(function ($body) {
                    $params = json_decode($body, true);
                    return $params['time_to_live'] === 60;
                })
            )
            ->will(static::returnValue($response));

        $message = new Message();
        $message->setTitle('title')
            ->setBody('body')
            ->setIcon('icon')
            ->setClickAction('click action');

        $this->notifier->notifyTopic($message, 'test', 60);
    }

    protected function setUp()
    {
        $this->browser = $this->getMockBuilder(Browser::class)
            ->getMock();

        $this->notifier = new Notifier($this->browser, '');
    }
}
